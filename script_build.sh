#!/usr/bin/env bash

user=jenkins
BUILD_PATH="out/target/product/$device"
DATE=$(date '+%d-%b-%Y')

# Export colors
export TERM=xterm

    red=$(tput setaf 1) # red
    grn=$(tput setaf 2) # green
    ylw=$(tput setaf 3) #  yellow
    blu=$(tput setaf 4) # blue
    cya=$(tput setaf 6) # cyan
    txtrst=$(tput sgr0) # Reset

#Reset trees & Sync with latest source
if [ "$reset" = "yes" ];
then
rm -rf .repo/local_manifests
echo "Resetting current working tree...."
repo forall -c "git reset --hard" > /dev/null
echo "Reset complete!"
repo forall -c "git clean -f -d"
echo "Syncing latest sources"
repo sync --force-sync --no-tags --no-clone-bundle
fi

# Export ccache
if [ "$use_ccache" = "yes" ];
then
echo -e ${blu}"CCACHE is enabled for this build"${txtrst}
export CCACHE_EXEC=$(which ccache)
export USE_CCACHE=1
export CCACHE_COMPRESS=1
export CCACHE_DIR=/var/lib/jenkins/ccache/$user
ccache -M 75G
fi

#Clear ccache
if [ "$use_ccache" = "clean" ];
then
export CCACHE_EXEC=$(which ccache)
export CCACHE_DIR=/var/lib/jenkins/ccache/$user
ccache -C
export USE_CCACHE=1
export CCACHE_COMPRESS=1
ccache -M 75G
wait
echo -e ${grn}"CCACHE Cleared"${txtrst};
fi

#Export User and host variables
export KBUILD_BUILD_USER=StagOS
export KBUILD_BUILD_HOST=Jenkins

#Weekly
if [ "build_weekly" = "true" ];
then
export STAG_WEEKLY=true
else
#Build Official
export BUILD_TYPE=OFFICIAL
fi

# clean
if [ "$make_clean" = "yes" ];
then
make clean && make clobber
wait
echo -e ${cya}"OUT dir from your repo deleted"${txtrst};
fi

if [ "$make_clean" = "installclean" ];
then
make installclean
wait
echo -e ${cya}"Images deleted from OUT dir"${txtrst};
fi

#export gdrive folderid
if [ "${device}" == "mido" ];
then
folderid=1GNQHnCUcscFQMagEHD-zaUoqsU5DAbab
maintainer="@SeekNDstroy"
devicename="Redmi Note 4"
fi

if [ "${device}" == "beryllium" ];
then
folderid=1pCSZcd2oIyImznYSUpQkQCj-5ld_50GF
maintainer=
devicename="Xiaomi Poco F1"
fi

if [ "${device}" == "chiron" ];
then
folderid=15JGRrPh32dBv-Udx2V2goBB3LQFsP7KG
maintainer="@Lightvortex242"
devicename="Mi Mix 2"
fi

if [ "${device}" == "dipper" ];
then
folderid=1LY8v25qTzLnNe6es1oGnA1ixQ-uxxn_y
fi

if [ "${device}" == "enchilada" ];
then
folderid=1F-DcgMPr0rVOYsWRmOdawgfrTELRjr9N
maintainer="@vjspranav"
devicename="Oneplus 6"
fi

if [ "${device}" == "fajita" ];
then
folderid=1Ah-Q3Xz6PtBc1WuARjy2aUdP_xVWv9wu
maintainer="@vjspranav"
devicename="Oneplus 6T"
fi

if [ "${device}" == "garlic" ];
then
folderid=1Tdg4kYcwjSetnh2Ge6uoTi1UblATJL9G
maintainer="@vjspranav"
devicename="Yu Yureka Black"
fi

if [ "${device}" == "violet" ];
then
folderid=1UJNGdD-64jCYfVA84q3gT544NGMJ6JnP
maintainer="@tesla59"
devicename="Redmi Note 7 Pro"
fi

if [ "${device}" == "wayne" ];
then
folderid=1Pq0QHuqyqWtSorng3U_kVSI13b-IeqOO
fi

if [ "${device}" == "whyred" ];
then
folderid=1Jh2p9BtW-WYOIPqcjI86OOZ0GfYEyz7B
maintainer="@NAHSEEZ"
devicename="Redmi Note 5 Pro"
fi

if [ "${device}" == "X00T" ];
then
folderid=1dQAjsvKsLJRN2djPVoHp7_HDfzt7jC1C
fi

if [ "${device}" == "z2_plus" ];
then
folderid=1kwSABaLcoK65CDzV8ryOj6tEKeiB0ain
maintainer="@nikhilgohil3"
devicename="Lenovo Zuk Z2"
fi

#Time to build
source build/envsetup.sh
lunch stag_${device}-"$build_type"
telegram-send --format html "Weekly build started for <b>${devicename} ($device)</b> on jenkins <a href='${BUILD_URL}console'>here</a>! ${maintainer}"
make bacon -j8

# If the above was successful
if [ `ls $BUILD_PATH/StagOS-${device}-10.0.1*.zip 2>/dev/null | wc -l` != "0" ];
then
   BUILD_RESULT="Build successful"
   telegram-send "Build successful for ${device}, uploading."

   #Upload to g drive
   cd $BUILD_PATH
   STAG="$(ls StagOS-${device}-10.0.1*.zip)"
   size=$(du -sh $STAG | awk '{print $1}')
   md5=$(md5sum $STAG | awk '{print $1}' )

   echo "Uploading to Google Drive"
   fileid=$(gdrive upload ${STAG} -p ${folderid} | tail -1 | awk '{print $2}')
   DRIVE="https://drive.google.com/uc?id=$fileid&export=download"
   LINK=https://downloads.stag.workers.dev/${device}/${STAG}

   echo "Uploading to Sourceforge"
   ~/sshpass -e sftp -oBatchMode=no -b - stag-maintainer@frs.sourceforge.net << !
     cd /home/frs/project/stagos-10/$device
     put ${STAG}
     bye
!

   SF=https://sourceforge.net/projects/stagos-10/files/${device}/${STAG}

read -r -d '' msg <<EOT
<b>Device: $device </b>
Download: <a href="${LINK}">Click Here</a> | <a href="${SF}">SF Mirror</a>
Maintainer: ${maintainer}
FileSize: $size
MD5: $md5
Build Date: $DATE
EOT

   telegram-send --format html --disable-web-page-preview "${msg}"

else

   BUILD_RESULT="Build failed"
   telegram-send "Build for ${device} failed. Check what caused your build to fail <a href='${BUILD_URL}console'>here</a>. ${maintainer}"
   exit 1
fi

cd ../../../../
